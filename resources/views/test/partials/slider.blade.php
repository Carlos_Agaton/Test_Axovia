<div class="slider">
    <ul class="slides">
        <li>
            <img src="images/lady-of-guadalupe-church-puerto-vallarta-002.jpg"> <!-- random image -->
            <div class="caption center-align">
                <h3>Axovia Adventures!</h3>
                <h5 class="light grey-text text-lighten-3">Llevate de vallarta buenos momentos.</h5>
            </div>
        </li>
        <li>
            <img src="images/adventure-f399548e3c7ce87126bba21df18cedf4.jpg"> <!-- random image -->
            <div class="caption left-align">
                <h3>Canopy</h3>
                <h5 class="light grey-text text-lighten-3">Vive la aventura.</h5>
            </div>
        </li>
        <li>
            <img src="images/stand-up-paddle.jpg"> <!-- random image -->
            <div class="caption right-align">
                <h3>Disfruta las paradisiacas playas de Valarta</h3>
                <h5 class="light grey-text text-lighten-3">Bienvenido al paraiso.</h5>
            </div>
        </li>
        <li>
            <img src="images/visita-puerto-vallarta-02.jpg"> <!-- random image -->
            <div class="caption center-align">
                <h3>Vida nocturna!</h3>
                <h5 class="light grey-text text-lighten-3">Vallarta Shore.</h5>
            </div>
        </li>
    </ul>
</div>