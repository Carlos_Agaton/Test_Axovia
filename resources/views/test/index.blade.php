@extends('test.master')
@section('contenido')
<!-- Slider -->
@include('test.partials.slider')
<!-- Fin del slider -->
<div class="container">
    <div id="axovia" class="row">
        <div class="col s12 l6">
            <div class="col s12 l12">
                <img class="responsive-img center-align" src="images/trip.png" alt="">
            </div>
            <div class="col s12 l12">
                <p align="justify">
                    Axovia Adventures es el operador de tours y  actividades numero uno en Puerto Vallarta.
                    Ofrecemos un amplia seleccion de aventuras, tours, excursiones y cosas por hacer para todas las edades
                    - desde tirolesas hasta nado con delfines, pasando por avistamiento de ballenas, snorkeling, buceo y una cena
                    que incluye un increible show.  Vive con nosotros alguna de las atracciones de Vallarta y descubre porque hemos
                    sido calificados como los ecotours numero uno de la region
                </p>
            </div>
        </div>
        <div class="col s12 l6">
            <div class="video-container responsive-video" controls>
                <iframe src="https://www.youtube.com/embed/fWIkKedNDfw" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div id="aventuras" class="row">
        <div class="col s12 l4 hoverable">
            <div class="col l12 s12">
                <img class="circle responsive-img" src="images/swim-with-dolphins-riviera-maya-LDTN.jpg" alt="">
            </div>
            <div class="s12 l12">
                <h5 class="center-align">Nado con delfines</h5>
            </div>
            <div class="col l12 s12">
                <p align="justify">Axovia Adventures ofrece experiencias de nivel mundial al nadar con delfines en el hermoso
                    Vallarta. Localizado en la marina de Nuevo Vallarta, nuestro delfinario te ofrece instalaciones incomparables
                    para disfrutar al maximo de tu experiencia con delfines.
                </p>
            </div>
        </div>
        <div class="col s12 l4 hoverable">
            <div class="col l12 s12">
                <img class="circle responsive-img" src="images/canopy-1-Custom.jpg" alt="">
            </div>
            <div class="s12 l12">
                <h5 class="center-align">Canopy</h5>
            </div>
            <div class="s12 l12">
                <p align="justify">
                    Este tour comienza con un circuito de tirolesas muy divertidas, entre ellas una doble para retar a quien tu quieras.
                    En el recorrido viviras momentos de gran emocion a unos 30 mts., de altura sobre la copa de los arboles y gritaras como
                    el gran Tarzan que llevas dentro mientras giras en una tirolesa increible.
                </p>
            </div>
        </div>
        <div class="col s12 l4 hoverable">
            <div class="col l12 s12">
                <img class="circle responsive-img" src="images/paseos_naturalezaop.JPG" alt="">
            </div>
            <div class="s12 l12">
                <h5 class="center-align">Paseos en la naturaleza</h5>
            </div>
            <div class="col l12 s12">
                <p align="justify">Si eres de las personas que les apasiona explorar las riquezas del lugar que visitan y adentrarse en
                    su cultura, este tour es para ti. Conoceras el hermoso jardin botanico, el pintoresco pueblo de El Tuito y sus
                    calles coloniales que datan del siglo 16.
                </p>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <br>
    <div id="salidas" class="row grey lighten-4 card-panel">
        <div class="col s12 l4">
            <h4>Que necesito</h4>
            <div class="col s12">
                <p>- Personas embarazadas no pueden nadar con delfines.</p>
                <p>- Dinero</p>
                <p>- 3 pelos de unicornio</p>
                <p>- Mujeres deberan ir depiladas.</p>
            </div>
        </div>
        <div class="col s12 l4">
            <h4>Como vesitrse</h4>
            <div class="col s12">
                <p>- Roja ligera y tenis comodos</p>
                <p>- Repelente para insectos</p>
                <p>- Las camaras no son permitidas por razones de seguridad</p>
            </div>
        </div>
        <div class="col s12 l4">
            <h4>Salidas</h4>
            <div class="col s12">
                <p>- Nuevo Vallarta (Av. Mexico #570) horarios de salida: </p>
                <p>7:50 Hrs | 8:50 Hrs. | 9:50 Hrs.</p>
            </div>
            <div class="col s12">
                <p>- Marina (Collage Disco) horarios de salida: </p>
                <p>7:50 Hrs | 8:50 Hrs. | 9:50 Hrs.</p>
            </div>
            <div class="col s12">
                <p>- Plaza las glorias (Fco. Medina Ascencio #157) horarios de salida: </p>
                <p>7:50 Hrs | 8:50 Hrs. | 9:50 Hrs.</p>
            </div>
            <div class="col s12">
                <p>- Centro (Insuergentes #1070) horarios de salida: </p>
                <p>7:50 Hrs | 8:50 Hrs. | 9:50 Hrs.</p>
            </div>
        </div>
    </div>
</div>
@endsection