<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Axovia</title>
</head>
<body>
<!--Navbar-->
<div class="navbar-fixed">
    <nav>
        <div class="nav-wrapper red accent-4">
            <a href="/" class="brand-logo"><img src="images/LOGO-MENU-INF-21.png" alt=""></a>
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="#axovia">Axovia Adventures</a></li>
                <li><a href="#aventuras">Aventuras</a></li>
                <li><a href="#salidas">Salidas</a></li>
                <li><a href="contacto">Contacto<i class="material-icons left">call</i></a></li>
            </ul>
            <ul class="side-nav" id="mobile-demo">
                <li><a href="#axovia">Axovia Adventures</a></li>
                <li><a href="#aventuras">Aventuras</a></li>
                <li><a href="#salidas">Salidas</a></li>
                <li><a href="contacto">Contacto<i class="material-icons left">call</i></a></li>
            </ul>
        </div>
    </nav>
</div>
<!-- Fin del Navbar -->

@yield('contenido')

<!-- Footer -->
<footer class="page-footer grey lighten-3">
    <div class="footer-copyright">
        <div class="container">
            <b class="black-text text-ligthen-4">� 2015 Copyright Carlos Agaton</b>
        </div>
    </div>
</footer>
<!-- Fin del footer -->
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<!-- SideNav -->
<script>
    $(".button-collapse").sideNav();
</script>
<!-- Slider -->
<script !src="">
    $(document).ready(function(){
        $('.slider').slider(
                {
                    full_width: true,
                    height: 550
                });
    });
</script>
<script>
    $(document).ready(function(){
        $('.scrollspy').scrollSpy();
    });
</script>
@yield('scripts')
</body>
</html>