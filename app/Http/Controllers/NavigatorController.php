<?php namespace Axovia\Http\Controllers;

use Axovia\Http\Requests;
use Axovia\Http\Controllers\Controller;

use Illuminate\Http\Request;

class NavigatorController extends Controller {

    public function index()
    {
        return view('test.index');
    }

    public function contacto()
    {
        return view('test.contacto');
    }

}
